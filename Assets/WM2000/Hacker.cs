﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hacker : MonoBehaviour
{
    // Game State
    int level;
    string password;
    string[] level1Passwords = { "book", "aisle", "shelf", "media", "fiction" };
    string[] level2Passwords = { "uniform", "handcuffs", "criminal", "firearm", "constable" };
    string[] level3Passwords = { "atmosphere", "telescope", "starfield", "blackhole", "astronaut" };

    enum Screen { MainMenu, Password, Win};
    Screen currentScreen;

    // Start is called before the first frame update
    void Start()
    {
        ShowMainMenu();
    }
    
    void ShowMainMenu()
    {
        currentScreen = Screen.MainMenu;
        Terminal.ClearScreen();
        Terminal.WriteLine("Hey man");
        Terminal.WriteLine("Where would you like to hack into?");
        Terminal.WriteLine("Press 1 for Public Library");
        Terminal.WriteLine("Press 2 for Police Station");
        Terminal.WriteLine("Press 3 for NASA");
    }

    void OnUserInput(string input)
    {
        if (input == "menu")
        {
            ShowMainMenu();
        }
        else if (currentScreen == Screen.MainMenu)
        {
            RunMainMenu(input);
        }
        else if (currentScreen == Screen.Password)
        {
            CheckPassword(input);
        }
    }


    void RunMainMenu(string input)
    {
        bool isValidLevelNumber = (input == "1" || input == "2" || input == "3");
        if (isValidLevelNumber)
        {
            level = int.Parse(input);
            AskForPassword();
        }
        else if (input == "007")
        {
            Terminal.WriteLine("Welcome Mr.Bond, please pick a level");
        }
        else if (input == "menu")
        {
            ShowMainMenu();
        }
        else
        {
            Terminal.WriteLine("Please choose a valid level");
        }
    }

    void AskForPassword()
    {
        currentScreen = Screen.Password;
        Terminal.ClearScreen();
        Terminal.WriteLine("You have chosen level " + level);
        SetRandomPassword();
        Terminal.WriteLine("Please enter the password");
        Terminal.WriteLine("Hint:" + password.Anagram());
        Terminal.WriteLine("Type menu to return to main menu");
    }

    private void SetRandomPassword()
    {
        switch (level)
        {
            case 1:
                password = level1Passwords[Random.Range(0, level1Passwords.Length)];
                break;
            case 2:
                password = level2Passwords[Random.Range(0, level2Passwords.Length)];
                break;
            case 3:
                password = level3Passwords[Random.Range(0, level3Passwords.Length)];
                break;
            default:
                Debug.LogError("Inavlid level number"); //catchall
                break;
        }
    }

    void CheckPassword(string input)
    {
        if (input == password)
        {
            DisplayWin();
        }
        else if (input == "menu")
        {
            ShowMainMenu();
        }
        else
        {
            AskForPassword();
            // Terminal.WriteLine("Error. Wrong password");
        }
        
    }

    void DisplayWin()
    {
        currentScreen = Screen.Win;
        Terminal.ClearScreen();
        ShowLevelReward();
    }

    void ShowLevelReward()
    {
        switch (level)
        {
            case 1:
                Terminal.WriteLine("Well done, have a book.");
                Terminal.WriteLine(@"
      ________
    /        //  
   /        //
  /________//
 (________(/

");
                Terminal.WriteLine("Return by typing menu and try a harder challenge.");
                break;
            case 2:
                Terminal.WriteLine("You are now free");
                Terminal.WriteLine(@"
(╯°□°）╯︵ ┻━┻

");
                Terminal.WriteLine("Return by typing menu and try a harder challenge.");
                break;
            case 3:
                Terminal.WriteLine("Nuclear launch detected.");
                Terminal.WriteLine(@"
¯\_(ツ)_/¯
");
                break;
            default:
                Debug.LogError("Win screen not detected");
                break;
        }
    }
}


